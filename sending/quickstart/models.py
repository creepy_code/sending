from django.db import models
#import uuid
#from phonenumber_field.modelfields import PhoneNumberField
#import re

# Create your models here.

"""
A model of tag
"""

class Tag(models.Model):
    """Tag for data. Every tag has unique text.
    """
    text = models.CharField(max_length=64, unique=True)

    def __str__(self):
        return 'Tag[id: {id}, text: {text}]'.format(
            id=self.id, text=self.text)

"""
A model of library
"""

class Library(models.Model):
    """Library represents a piece of software. It has its website url,
    name, description and as many tags as you like.
    """

    # django has a nice field that validates URLs
    url = models.URLField()

    # name should be in a slug form
    name = models.SlugField(max_length=80)
    description = models.CharField(max_length=256, blank=True)
    tags = models.ManyToManyField(Tag, related_name='libraries')

    def __str__(self):
        return 'Library[id: {id}, name: {name}]'.format(
            id=self.id, name=self.name)


class Message_object(object):
    """docstring for Message_object."""
    id_message = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    sent_timedate = models.DateTimeField(auto_now=True, auto_now_add=True, **options)
    sending_status = models.BooleanField(**options)
    id_sending = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    id_client = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    def __init__(self, arg):
        super(Message_object, self).__init__()
        self.arg = arg

    def __str__(self):
        

class Sending_object(object):
    """docstring for Sending_object."""
    id_sending = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    sending_timedate = models.DateTimeField(auto_now=True, auto_now_add=True, **options)
    message_string = models.CharField(max_length=None, **options)


    def __init__(self, arg):
        super(Sending_object, self).__init__()
        self.arg = arg

class Client_object(object):
    """docstring for Client_object."""
    id_client = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    #client_number = PhoneNumberField(PHONENUMBER_DB_FORMAT = "NATIONAL", PHONENUMBER_DEFAULT_REGION = "RU", PHONENUMBER_DEFAULT_FORMAT = "NATIONAL")
    client_number = models.PositiveBigInteger()
    provider_code = models.PositiveBigInteger()


    def __init__(self, arg):
        super(Client_object, self).__init__()
        self.arg = arg
